Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_ONION", function(loc)

	local texts = {}
	
	local file_names = {
		"mods/PW_PD2M__Polish/loc.json" 
	}

	for _,filename in pairs(file_names or {})do
		local fo = io.open(filename,"r")
		if fo then
			local txt = fo:read("*a")
				io.close(fo)
			if txt and type(txt) == "string" then
				--local data = assert(loadstring("local text = {\n"..txt.."\n}return text"))()--lua table
				local data = json.decode(txt)--json
				if data and type(data) == "table" then
					for i,v in pairs(data)do
						if v ~= "" then
							texts[i] = v
						end
					end
				end
			end
		end
	end
	loc:add_localized_strings(texts)
end
)

function LocalizationManager.text( self, str, macros )

	-- log( "Localizer: " .. tostring(Localizer.__index) )
	-- log( "SystemInfo:language():key(): " )
	-- lang_mods[Idstring("german"):key()]
	-- lang_mods[Idstring("french"):key()]
	-- lang_mods[Idstring("italian"):key()]
	-- lang_mods[Idstring("spanish"):key()]
	-- lang_mods[Idstring("english"):key()]

	if self._custom_localizations[str] then
		local return_str = self._custom_localizations[str]
		if macros and type(macros) == "table" then
			for k, v in pairs( macros ) do
				return_str = return_str:gsub( "$" .. k .. ";", "$" .. k .. ";")
			end
		end
		self._macro_context = macros
		return_str = self:_localizer_post_process( return_str )
		self._macro_context = nil
		return return_str
	end
	return self.orig.text(self, str, macros)

end